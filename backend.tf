terraform {
  backend "s3" {
    bucket = "terraform-s3-state-223"
    key    = "my-terraform-project"
    region = "us-east-1"
    dynamodb_table   = "terraform-state-lock-dynamo"
    
  }
}
